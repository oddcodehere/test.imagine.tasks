package test.imagine.tasks.animate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class AnimationTest {

    private static final Object[][] FINAL_TEST_CASES = new Object[][]{
            {"..R....", new String[]{
                    "..X....",
                    "....X..",
                    "......X",
                    "......."
            }, 2},
            {"RR..LRL", new String[]{
                    "XX..XXX",
                    ".X.XX..",
                    "X.....X",
                    "......."
            }, 3},
            {"LRLR.LRLR", new String[]{
                    "XXXX.XXXX",
                    "X..X.X..X",
                    ".X.X.X.X.",
                    ".X.....X.",
                    "........."
            }, 2},
            {"RLRLRLRLRL", new String[]{
                    "XXXXXXXXXX",
                    ".........."
            }, 10},
            {"...", new String[]{
                    "..."
            }, 1},
            {"LRRL.LR.LRR.R.LRRL.",
                    new String[]{
                            "XXXX.XX.XXX.X.XXXX.",
                            "..XXX..X..XX.X..XX.",
                            ".X.XX.X.X..XX.XX.XX",
                            "X.X.XX...X.XXXXX..X",
                            ".X..XXX...X..XX.X..",
                            "X..X..XX.X.XX.XX.X.",
                            "..X....XX..XX..XX.X",
                            ".X.....XXXX..X..XX.",
                            "X.....X..XX...X..XX",
                            ".....X..X.XX...X..X",
                            "....X..X...XX...X..",
                            "...X..X.....XX...X.",
                            "..X..X.......XX...X",
                            ".X..X.........XX...",
                            "X..X...........XX..",
                            "..X.............XX.",
                            ".X...............XX",
                            "X.................X",
                            "..................."
                    }, 1},
    };

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(FINAL_TEST_CASES);
    }

    private String init;
    private String[] results;
    private int speed;

    public AnimationTest(String init, String[] results, int speed) {
        this.speed = speed;
        this.results = results;
        this.init = init;
    }

    @Test
    public void testAnimation() {
        System.out.println("------");
        System.out.println("init: " + init + " speed: " + speed);
        List<String> collectedResults = new ArrayList<>();
        Animation animation = new Animation(new NestedAnimationRenderer(new DefaultAnimationRenderer()) {
            @Override
            public char[] render(char[] animated) {
                char[] rendered = super.render(animated);
                System.out.println(rendered);
                collectedResults.add(new String(rendered));
                return rendered;
            }
        });
        animation.animate(speed, init);
        assertArrayEquals("", results, collectedResults.toArray(new String[0]));
    }
}
