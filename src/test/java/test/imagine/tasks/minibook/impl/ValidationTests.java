package test.imagine.tasks.minibook.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import test.imagine.tasks.minibook.QuoteFormatValidationException;

import java.util.Arrays;
import java.util.Collection;
import java.util.function.Function;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@RunWith(Parameterized.class)
public class ValidationTests {
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {null, testErrorCase},
                {"", testErrorCase},
                {"Q1", testErrorCase},
                {"Q1/O", testErrorCase},
                {"Q1/O/N", testErrorCase},
                {"Q1/O/N/1.31", testErrorCase},

                {"Q1/O/N/1.31/1000000", testValidFormatCase},
                {"Q7/O/D/0/0", testValidFormatCase},
        });
    }

    private String quote;
    private Function<String, Void> testCase;

    public ValidationTests(String quote, Function<String, Void> testCase) {
        this.quote = quote;
        this.testCase = testCase;
    }

    @Test
    public void testValidator() {
        testCase.apply(quote);
    }

    public static void validate(String quote) {
        MiniBookImpl miniBook = new MiniBookImpl();
        miniBook.validate(quote, miniBook.parse(quote));
    }

    public static final Function<String, Void> testValidFormatCase = (quote) -> {
        try {
            validate(quote);
        } catch (Exception e) {
            fail(String.format("There shouldn't be any exception thrown %s", quote));
        }
        return null;
    };

    public static final Function<String, Void> testErrorCase = (quote) -> {
        try {
            validate(quote);
            fail(String.format("Exception should be thrown in error case for %s", quote));
        } catch (Exception e) {
            assertTrue(String.format("Validator should throw %s", QuoteFormatValidationException.class.getName()),
                    e instanceof QuoteFormatValidationException);
        }
        return null;
    };


}